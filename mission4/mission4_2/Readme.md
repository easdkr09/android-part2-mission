# 피트니스 앱의 활동 로그 도넛 작성 

### 요구 사항 
1. 화면 구성
2. SeekBar로 도넛 부분 조정 

### 구성
- java \
CallLogAdapter.java : listView를 위한 어댑터 구현, listView item 초기화 \
CallLogVO.java : 사용자 call log 정보를 담는 클래스  \
CallLogWrapper.java : listView의 뷰 객체들을 담는 클래스 \
DBHelper.java : DB 생성, 데이터 삽입 \
MainActivity.java : 퍼미션 체크, 요청, db select, vo array 초기화, 어댑터 연결

- layout \
activity_main.xml \
main_list_item.xml

### 참조 코드
크기 리소스 등록은 dimens.xml을 이용하고, 이곳에서 dp 단위로 등록한 것을 코드에서 픽셀로 획득하여 이용한다. 
```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <dimen name="donut_size">200dp</dimen>
    <dimen name="donut_stroke_size">15dp</dimen>
    <dimen name="donut_textSize">20dp</dimen>
</resources>
```

커스텀 뷰의 크기 획득
```java
 @Override
protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    this.width = MeasureSpec.getSize(widthMeasureSpec);
    this.height = MeasureSpec.getSize(heightMeasureSpec);
}
```

커스텀 뷰의 센터에 문자열 그리기 
```java
String txt=String.valueOf(value);
int xPos=width/2 - (int)(paint.measureText(txt)/2);
int yPos=(int)(height/2 - ((paint.descent()+paint.ascent())/2));

canvas.drawText(txt, xPos, yPos, paint);
```

### 추가적인 메모
```java
setStrokeJoin : 사각형의 끝선 타입 지정(ex. 둥글게할지.. )
```