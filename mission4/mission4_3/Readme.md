# 검색 추천단어 화면 구성 

### 요구 사항 
1. 화면 구성 : EditText로 사용자로 부터 입력 받고 추천 단어들은 리스트뷰로 출력
2. 추천 단어를 선택하면 선택된 문자열 전체가 EditText에 찍히고, ListView는 사라져야한다. 
3. 입련한 단어가 포함된 문자열을 ListView에 표현하고, 사용자가 입력한 검색어의 단어가 추천 단어 부분에서 굵은 파란색으로 표현되어야 한다. 
### 참조 코드
- 아이템 클릭 시 ListView가 사라지게하기 (교재 github과 다름)
```java
@Override
public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
    this.editText.setText(datas.get(position).toString());
    listView.setAdapter(null);
}
```
- 텍스트가 변경될 때 이벤트
```java
    //TextWatcher를 액티비티에서 구현 

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        this.datas = new ArrayList<>();
        //굵은 파란색으로 표현하는 부분
        for(String recommendStr : base){
            if(recommendStr.contains(charSequence)){
                int startPos = recommendStr.indexOf(charSequence.toString());
                int endPos = startPos + charSequence.length();
                SpannableStringBuilder builder = new SpannableStringBuilder(recommendStr);
                builder.setSpan(new ForegroundColorSpan(Color.BLUE),
                        startPos, endPos,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                builder.setSpan(new StyleSpan(Typeface.BOLD),
                        startPos, endPos,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                datas.add(builder);
            }
        }
        ArrayAdapter<SpannableStringBuilder> adapter = new ArrayAdapter<SpannableStringBuilder>(this,
                android.R.layout.simple_list_item_1,datas);
        listView.setAdapter(adapter);
    }
```
### 추가적인 메모
Spanned의 옵션들을 정확하게 알아둬야할 필요가 있을거 같다(여러가지 테스트 후 내용 추가할 것)