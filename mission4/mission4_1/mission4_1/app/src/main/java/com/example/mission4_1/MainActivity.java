package com.example.mission4_1;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String selectQuery = "SELECT name, photo, date, phone from tb_call_log";
    boolean callPermission;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //퍼미션 체크하는 부분
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED)
            callPermission= true;
        //사용자에게 퍼미션 요청
        if(!callPermission)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 200);

        ListView listView = findViewById(R.id.main_list);
        ArrayList<CallLogVO> datas = new ArrayList<>();

        //DB의 정보들을 datas에 담아준다.
        DBHelper helper = new DBHelper(this);
        SQLiteDatabase db= helper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        while(cursor.moveToNext()){
            CallLogVO vo = new CallLogVO();
            vo.name = cursor.getString(0);
            vo.photo = cursor.getString(1);
            vo.date = cursor.getString(2);
            vo.phone = cursor.getString(3);
            datas.add(vo);
        }
        db.close();

        CallLogAdapter adapter = new CallLogAdapter(this, R.layout.main_list_item, datas);
        listView.setAdapter(adapter);
    }
}