package com.example.mission4_1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 2;

    public DBHelper(Context context){
        super(context, "calldb", null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createTableSQL = "create table tb_call_log("+
                "id integer primary key autoincrement," +
                "name not null," +
                "photo," +
                "date," +
                "phone)";
        sqLiteDatabase.execSQL(createTableSQL);

        sqLiteDatabase.execSQL("insert into tb_call_log (name, photo, date, phone) values ('홍길동','yes','휴대전화, 1일전','010001')");
        sqLiteDatabase.execSQL("insert into tb_call_log (name, photo, date, phone) values ('류현진','no','휴대전화, 1일전','010001')");
        sqLiteDatabase.execSQL("insert into tb_call_log (name, photo, date, phone) values ('강정호','no','휴대전화, 2일전','010001')");
        sqLiteDatabase.execSQL("insert into tb_call_log (name, photo, date, phone) values ('김현수','yes','휴대전화, 2일전','010001')");
        sqLiteDatabase.execSQL("insert into tb_call_log (name, photo, date, phone) values ('오승환','no','휴대전화, 2일전','010001')");
        sqLiteDatabase.execSQL("insert into tb_call_log (name, photo, date, phone) values ('이대호','no','휴대전화, 3일전','010001')");
        sqLiteDatabase.execSQL("insert into tb_call_log (name, photo, date, phone) values ('박병호','no','휴대전화, 3일전','010001')");
        sqLiteDatabase.execSQL("insert into tb_call_log (name, photo, date, phone) values ('추신수','no','휴대전화, 3일전','010001')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if(newVersion == DATABASE_VERSION){
            sqLiteDatabase.execSQL("drop table tb_call_log");
            onCreate(sqLiteDatabase);
        }
    }
}
