package com.example.mission4_1;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CallLogWrapper {
    public ImageView personImageView;
    public TextView nameView;
    public TextView dateView;
    public ImageView dialerImageView;
    public CallLogWrapper(View v){
        personImageView= v.findViewById(R.id.main_item_person);
        nameView = v.findViewById(R.id.main_item_name);
        dateView = v.findViewById(R.id.main_item_date);
        dialerImageView = v.findViewById(R.id.main_item_dialer);
    }
}
