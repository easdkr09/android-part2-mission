# 전화 앱의 로그 화면 구성 

### 요구 사항 
1. 퍼미션 조정
2. 화면 구성
3. 전체 화면 구성 및 전화 걸기 이벤트

### 구성
- java \
CallLogAdapter.java : listView를 위한 어댑터 구현, listView item 초기화 \
CallLogVO.java : 사용자 call log 정보를 담는 클래스  \
CallLogWrapper.java : listView의 뷰 객체들을 담는 클래스 \
DBHelper.java : DB 생성, 데이터 삽입 \
MainActivity.java : 퍼미션 체크, 요청, db select, vo array 초기화, 어댑터 연결

- layout \
activity_main.xml \
main_list_item.xml

### 참조 코드
- 전화 걸기
```java
Intent intent = new Intent();
intent.setAction(Intent.ACTION_CALL);
intent.setData(Uri.parse("tel:"+vo.phone));
context.startActivitiy(intent);
```